package com.gitrekt.quora.models;

public class Star {

    private String userid;


    private String answerid;

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getAnswerid() {
        return answerid;
    }

    public void setAnswerid(String answerid) {
        this.answerid = answerid;
    }


    @Override
    public String toString() {
        return "Star{" +
                "userid='" + userid + '\'' +
                ", answerid='" + answerid + '\'' +
                '}';
    }
}
