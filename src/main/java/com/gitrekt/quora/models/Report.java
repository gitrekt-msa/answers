package com.gitrekt.quora.models;

public class Report {
    String userid;
    String answerid;
    String reporttext;

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getAnswerid() {
        return answerid;
    }

    public void setAnswerid(String answerid) {
        this.answerid = answerid;

    }

    public String getReporttext() {
        return reporttext;
    }

    public void setReporttext(String reporttext) {
        this.reporttext = reporttext;
    }

    @Override
    public String toString() {
        return "Report{" +
                "userid='" + userid + '\'' +
                ", answerid='" + answerid + '\'' +
                ", reporttext='" + reporttext + '\'' +
                '}';
    }
}
