package com.gitrekt.quora.models;

public class BestAnswer {


    private String questionid;
    private String answerid;
    private String userid;

    public String getQuestionid() {
        return questionid;
    }

    public String getAnswerid() {
        return answerid;
    }

    public void setQuestionid(String questionid) {
        this.questionid = questionid;
    }

    public void setAnswerid(String answerid) {
        this.answerid = answerid;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }


    @Override
    public String toString() {
        return "BestAnswer{" + "questionid='" + questionid + "'," + "answerid='" + answerid +"'," + "userid='" + userid + '\'' + '}';
    }

}