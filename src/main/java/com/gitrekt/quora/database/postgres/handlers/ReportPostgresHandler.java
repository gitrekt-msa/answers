package com.gitrekt.quora.database.postgres.handlers;

import com.gitrekt.quora.models.Report;
import org.apache.commons.dbutils.BasicRowProcessor;
import org.apache.commons.dbutils.GenerousBeanProcessor;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

public class ReportPostgresHandler extends PostgresHandler<Report> {
    public ReportPostgresHandler() {
        super("answers_reports", Report.class);
    }

    public List<Report> getReports() {
        return super.findAll();
    }

    public void addReport(String userid, String answerid, String reporttext){
        try {
            PreparedStatement cs = connection.prepareCall("CALL Insert_Answer_Report(?,?,?)");
            cs.setObject(1, UUID.fromString(userid));
            cs.setObject(2, UUID.fromString(answerid));
            cs.setString(3, reporttext);

            cs.executeUpdate();
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
    }
}
