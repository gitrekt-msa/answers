package com.gitrekt.quora.database.postgres.handlers;

import com.gitrekt.quora.models.BestAnswer;
import org.apache.commons.dbutils.BasicRowProcessor;
import org.apache.commons.dbutils.GenerousBeanProcessor;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

public class BestAnswerPostgresHandler extends PostgresHandler<BestAnswer> {


    public BestAnswerPostgresHandler() {
        super("question_best_answers", BestAnswer.class);
    }

    public List<BestAnswer> InsertBestAnswer(String questionid, String answerid, String userid){
        try {
            //ResultSetHandler<List<BestAnswer>> elements = new BeanListHandler<BestAnswer>(mapper, new BasicRowProcessor(new GenerousBeanProcessor()));
            //return runner.query(String.format("CALL Insert_Best_Answer(%s,%s)", questionid, answerid), null);
            PreparedStatement cs = connection.prepareCall("CALL Insert_Best_Answer(?,?,?)");

            cs.setObject(1, UUID.fromString(questionid));
            cs.setObject(2, UUID.fromString(answerid));
            cs.setObject(3, UUID.fromString(userid));
            
            cs.executeUpdate();

        } catch (
                SQLException exception) {
            exception.printStackTrace();
        }
        return null;
    }

    public List<BestAnswer> DeleteBestAnswer(String questionid, String userid){
        try {
            //return runner.query(String.format("CALL Delete_Best_Answer(%s)", questionid), null);
            PreparedStatement cs = connection.prepareCall("CALL Delete_Best_Answer(?,?)");

            cs.setObject(1, UUID.fromString(questionid));
            cs.setObject(2, UUID.fromString(userid));
            cs.executeUpdate();

        } catch (
                SQLException exception) {
            exception.printStackTrace();
        }
        return null;
    }

}
