package com.gitrekt.quora.database.postgres.handlers;

import com.gitrekt.quora.models.Star;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

public class StarPostgresHandler extends PostgresHandler<Star> {
    public StarPostgresHandler() {
            super("answers_stars", Star.class);
    }

    public List<Star> getStars() {
        return super.findAll();
    }

    public void addStar(String userid, String answerid) throws SQLException {
        try {
            PreparedStatement cs = connection.prepareCall("CALL Insert_Answer_Star(?,?)");
            cs.setObject(1, UUID.fromString(userid));
            cs.setObject(2, UUID.fromString(answerid));
            cs.executeUpdate();
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
    }

    public void deleteStar(String userid, String answerid) throws SQLException {
        try {
            PreparedStatement cs = connection.prepareCall("CALL Delete_Answer_Star(?,?)");
            cs.setObject(1, UUID.fromString(userid));
            cs.setObject(2, UUID.fromString(answerid));
            cs.executeUpdate();
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
    }
}
