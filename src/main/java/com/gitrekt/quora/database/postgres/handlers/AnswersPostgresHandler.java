package com.gitrekt.quora.database.postgres.handlers;

import com.gitrekt.quora.models.Answer;
import com.google.gson.JsonParser;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.UUID;

public class AnswersPostgresHandler extends PostgresHandler<Answer> {

  public AnswersPostgresHandler() {
    super("Answer", Answer.class);
  }


  public String insertAnswer(String id, String questionId, String discussionId, String userId, String answerText, int stars, boolean isPublic, String media) throws SQLException {

    PreparedStatement cs = connection.prepareCall("CALL Insert_Answer(?,?,?,?,?,?,?,?::jsonb)");
    cs.setObject(1, UUID.fromString(id));
    cs.setObject(2, questionId != null ? UUID.fromString(questionId) : null);
    cs.setObject(3, discussionId != null ? UUID.fromString(discussionId) : null);
    cs.setObject(4, UUID.fromString(userId));
    cs.setObject(5, answerText);
    cs.setObject(6, stars);
    cs.setObject(7, isPublic);
    cs.setObject(8, media);
    cs.executeUpdate();
    return "Success";
  }


  public String deleteAnswer(String id, String userId) throws SQLException {

    PreparedStatement cs = connection.prepareCall("CALL Delete_Answer(?,?)");
    cs.setObject(1, UUID.fromString(id));
    cs.setObject(2, UUID.fromString(userId));
    cs.executeUpdate();
    return "Success";

  }

  public Answer getAnswer(String id) throws SQLException {

    String q = "SELECT * FROM answers WHERE id = ?";
    PreparedStatement cs = connection.prepareCall(q);
    cs.setObject(1, UUID.fromString(id));
    ResultSet resultSet = cs.executeQuery();

    if (resultSet == null || !resultSet.next()) {
      return null;
    }


    Answer answer = new Answer();
    answer.setId(resultSet.getString("id"));
    answer.setQuestionId(resultSet.getString("question_id"));
    answer.setDiscussionId(resultSet.getString("discussion_id"));
    answer.setUserId(resultSet.getString("user_id"));
    answer.setAnswerText(resultSet.getString("answer_text"));
    answer.setStars(resultSet.getInt("stars"));
    answer.setPublic(resultSet.getBoolean("is_public"));
    answer.setMedia(new JsonParser()
            .parse(resultSet.getString("media"))
            .getAsJsonObject()
    );
    answer.setCreatedAt(resultSet.getDate("created_at"));
    answer.setUpdatedAt(resultSet.getDate("updated_at"));
    answer.setDeletedAt(resultSet.getDate("deleted_at"));

    return answer;
  }
}
