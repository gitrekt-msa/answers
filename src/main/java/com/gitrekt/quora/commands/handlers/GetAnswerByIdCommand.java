package com.gitrekt.quora.commands.handlers;

import com.gitrekt.quora.commands.Command;
import com.gitrekt.quora.database.postgres.handlers.AnswersPostgresHandler;
import com.gitrekt.quora.exceptions.BadRequestException;
import com.gitrekt.quora.exceptions.NotFoundException;
import com.gitrekt.quora.models.Answer;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;

import java.sql.SQLException;
import java.util.HashMap;

public class GetAnswerByIdCommand extends Command {

  public GetAnswerByIdCommand(HashMap<String, Object> args) {
    super(args);
  }

  private static final String[] argumentNames = new String[] {"answerId"};

  @Override
  public Object execute() throws NotFoundException, BadRequestException, SQLException {
    System.out.println(args);
    checkArguments(argumentNames);
    String id;
    if(args.get("answerId") instanceof JsonPrimitive)
     id = ((JsonPrimitive )args.get("answerId")).getAsString();
    else
      id = (String) args.get("answerId");

    setPostgresHandler(new AnswersPostgresHandler());

    Answer answer = ((AnswersPostgresHandler) postgresHandler).getAnswer(id);

    JsonObject resBody = new JsonObject();
    if (answer.getId() == null) {
      throw new NotFoundException();
    }
    resBody.addProperty("answer_id", id);
    resBody.addProperty("question_id", answer.getQuestionId());
    resBody.addProperty("discussion_id", answer.getDiscussionId());
    resBody.addProperty("user_id", answer.getUserId());
    resBody.addProperty("answer_text", answer.getAnswerText());
    resBody.addProperty("stars", answer.getStars());
    resBody.addProperty("is_public", answer.isPublic());
    resBody.add("media", answer.getMedia());

    return resBody;
  }
}