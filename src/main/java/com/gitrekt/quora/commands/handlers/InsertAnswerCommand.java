package com.gitrekt.quora.commands.handlers;

import com.gitrekt.quora.commands.Command;
import com.gitrekt.quora.database.postgres.handlers.AnswersPostgresHandler;
import com.gitrekt.quora.exceptions.AuthenticationException;
import com.gitrekt.quora.exceptions.BadRequestException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;


import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.UUID;

public class InsertAnswerCommand extends Command {


  private static final String[] argumentNames = new String[]{
          "userId", "answerText"};

  public InsertAnswerCommand(HashMap<String, Object> args) {
    super(args);
  }

  protected void checkEntityId() throws BadRequestException{
    StringBuilder stringBuilder = new StringBuilder();
    int errorsCnt = 0;
    if (!args.containsKey("discussionId") || args.get("discussionId") == null) {
      stringBuilder.append(String.format("Argument %s is missing", "discussionId")).append("\n");
      errorsCnt++;
    }

    if (!args.containsKey("questionId") || args.get("questionId") == null) {
      stringBuilder.append(String.format("Argument %s is missing", "questionId")).append("\n");
      errorsCnt++;
    }

    if(errorsCnt != 1) {
      throw new BadRequestException(stringBuilder.toString());
    }

  }

  @Override
  public Object execute() throws SQLException, BadRequestException {
    checkArguments(argumentNames);
    checkEntityId();

    String id = UUID.randomUUID().toString();

    String questionId = (String) args.get("questionId");
    String discussionId = (String) args.get("discussionId");
    System.out.println(questionId + " " + discussionId);
    String userId = (String) args.get("userId");
    String answerText = (String) args.get("answerText");
    int stars = ((String) args.get("stars") == null)? 0: Integer.parseInt((String) args.get("stars"));
    boolean isPublic = ((String) args.get("isPublic") == null)? true: Boolean.parseBoolean((String) args.get("isPublic"));
    String media = (String) args.get("mediaVal");


    ((AnswersPostgresHandler) postgresHandler).insertAnswer(id, questionId, discussionId, userId, answerText, stars, isPublic, media);

    return "Answer Added Successfully";
  }
}
