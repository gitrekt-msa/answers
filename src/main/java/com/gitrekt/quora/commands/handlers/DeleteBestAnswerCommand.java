package com.gitrekt.quora.commands.handlers;

import com.gitrekt.quora.commands.Command;
import com.gitrekt.quora.database.postgres.handlers.BestAnswerPostgresHandler;
import com.gitrekt.quora.exceptions.BadRequestException;

import java.sql.SQLException;
import java.util.HashMap;


public class DeleteBestAnswerCommand extends Command {

    private static final String[] argumentNames = new String[]{"questionid","userId"};

    public DeleteBestAnswerCommand(HashMap<String,Object> args) {
        super(args);
    }

    @Override
    public String execute() throws SQLException, BadRequestException {
        checkArguments(argumentNames);

        String questionid = (String) args.get("questionid");
        String userid = (String) args.get("userId");

        ((BestAnswerPostgresHandler)postgresHandler).DeleteBestAnswer(questionid, userid);

        return "Best answer deleted successfully!";
    }
}

