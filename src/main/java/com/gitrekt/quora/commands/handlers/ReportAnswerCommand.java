package com.gitrekt.quora.commands.handlers;

import com.gitrekt.quora.commands.Command;
import com.gitrekt.quora.database.postgres.handlers.ReportPostgresHandler;
import com.gitrekt.quora.exceptions.BadRequestException;
import java.sql.SQLException;
import java.util.HashMap;

public class ReportAnswerCommand extends Command {

    private static final String[] argumentNames =
            new String[]{"userId","answerid","reporttext"};

    public ReportAnswerCommand (HashMap<String, Object> args) {
        super(args);
    }

    @Override
    public String execute() throws SQLException, BadRequestException {
        checkArguments(argumentNames);

        String userid = (String) args.get("userId");
        String answerid = (String) args.get("answerid");
        String reportext = (String) args.get("reporttext");

        ((ReportPostgresHandler)postgresHandler).addReport(userid, answerid, reportext);
        return "Report Added!";
    }


}
