package com.gitrekt.quora.commands.handlers;

import com.gitrekt.quora.commands.Command;
import com.gitrekt.quora.database.postgres.handlers.StarPostgresHandler;
import com.gitrekt.quora.exceptions.BadRequestException;

import java.sql.SQLException;
import java.util.HashMap;

public class DeleteStarCommand extends Command {

    private static final String[] argumentNames =
            new String[]{"userId","answerid"};

    public DeleteStarCommand(HashMap<String, Object> args) {
        super(args);
    }

    @Override
    public String execute() throws SQLException, BadRequestException {
        checkArguments(argumentNames);

        String userid = (String) args.get("userId");
        String answerid = (String) args.get("answerid");

        ((StarPostgresHandler)postgresHandler).deleteStar(userid, answerid);
        return "Answer deleted!";
    }

}