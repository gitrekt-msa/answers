package com.gitrekt.quora.commands.handlers;

import com.gitrekt.quora.commands.Command;
//import com.gitrekt.quora.database.postgres.handlers.BestAnswerPostgresHandler;
//import com.gitrekt.quora.database.postgres.handlers.UsersPostgresHandler;
import com.gitrekt.quora.database.postgres.handlers.BestAnswerPostgresHandler;
import com.gitrekt.quora.exceptions.BadRequestException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.UUID;

public class InsertBestAnswerCommand extends Command {

    private static final String[] argumentNames =
            new String[]{"questionid", "answerid","userId"};

    public InsertBestAnswerCommand(HashMap<String,Object> args) {
        super(args);
    }

    @Override
    public String execute() throws SQLException, BadRequestException {
        checkArguments(argumentNames);

        String questionid = (String) args.get("questionid");
        String answerid = (String) args.get("answerid");
        String userid = (String) args.get("userId");

        ((BestAnswerPostgresHandler)postgresHandler).InsertBestAnswer(questionid,answerid,userid);

        return "Best answer inserted successfully!";

    }
}
