package com.gitrekt.quora.commands.handlers;

import com.gitrekt.quora.commands.Command;
import com.gitrekt.quora.database.postgres.handlers.AnswersPostgresHandler;
import com.gitrekt.quora.exceptions.AuthenticationException;
import com.gitrekt.quora.exceptions.BadRequestException;
import com.google.gson.JsonObject;

import java.sql.SQLException;
import java.util.HashMap;

public class DeleteAnswerCommand extends Command {


    public DeleteAnswerCommand(HashMap<String, Object> args) {
        super(args);
    }

    private static final String[] argumentNames = new String[] {"id", "userId"};


    @Override
    public Object execute() throws SQLException, BadRequestException {
        checkArguments(argumentNames);

        String id = (String) args.get("id");
        String userId = (String) args.get("userId");

        ((AnswersPostgresHandler)postgresHandler).deleteAnswer(id, userId);

        JsonObject res = new JsonObject();
        res.addProperty("message","Answer deleted successfully");

        return res;

    }
}