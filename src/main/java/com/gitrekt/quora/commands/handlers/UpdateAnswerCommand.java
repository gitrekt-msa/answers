package com.gitrekt.quora.commands.handlers;

import com.gitrekt.quora.commands.Command;
import com.gitrekt.quora.database.postgres.handlers.AnswersPostgresHandler;
import com.gitrekt.quora.exceptions.AuthenticationException;
import com.gitrekt.quora.exceptions.BadRequestException;

import java.sql.SQLException;
import java.util.HashMap;

public class UpdateAnswerCommand extends Command {


    public UpdateAnswerCommand(HashMap<String, Object> args) {
        super(args);
    }

    private static final String[] argumentNames = new String[] {"id", "userId", "answerText"};


    @Override
    public Object execute() throws SQLException, BadRequestException {
        checkArguments(argumentNames);

        String id = (String) args.get("id");
        String answerText = (String) args.get("answerText");
        boolean isPublic = ((String) args.get("isPublic") == null)? true: Boolean.parseBoolean((String) args.get("isPublic"));
        int stars = ((String) args.get("stars") == null)? 0: Integer.parseInt((String) args.get("stars"));
        String media = (String) args.get("media");
        String userId = (String) args.get("userId");

        return ((AnswersPostgresHandler)postgresHandler).insertAnswer(id,null,null,userId, answerText,0, isPublic, media);
    }
}
