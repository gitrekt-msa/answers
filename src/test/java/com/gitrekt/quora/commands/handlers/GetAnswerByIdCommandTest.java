package com.gitrekt.quora.commands.handlers;

import com.gitrekt.quora.database.postgres.handlers.AnswersPostgresHandler;
import com.gitrekt.quora.exceptions.NotFoundException;
import com.gitrekt.quora.exceptions.ServerException;
import com.gitrekt.quora.models.Answer;
import com.google.gson.JsonObject;
import org.junit.Before;
import org.junit.Test;

import java.sql.SQLException;
import java.util.HashMap;

import static org.junit.Assert.*;

public class GetAnswerByIdCommandTest {

  protected HashMap<String, Object> args = new HashMap<>();

  @Before
  public void init() {
    args = new HashMap<>();
    args.put("userId", "0d50fcd3-eed6-4774-8ac0-41c0c2b72b0d");

  }


  @Test
  public void testGetAnswerById() throws ServerException, SQLException {

    args.put("id_val", "75c9c754-33c9-11e9-b210-d663bd873d93");

    GetAnswerByIdCommand cmd = new GetAnswerByIdCommand(args);


    JsonObject res = (JsonObject) cmd.execute();


    assertNotNull("No attribute should be null in the model",
            res.get("id"));
    assertEquals("UserId should be 0d50fcd3-eed6-4774-8ac0-41c0c2b72b0d",
            "0d50fcd3-eed6-4774-8ac0-41c0c2b72b0d",res.get("user_id"));
    assertNotNull("No attribute should be null in the model",
            res.get("question_id"));
    assertNotNull("No attribute should be null in the model",
            res.get("discussion_id"));
    assertNotNull("No attribute should be null in the model",
            res.get("user_id"));

    assertNull("Can not get model if deleted",
            res.get("deleted_at"));

  }

  @Test(expected = NotFoundException.class)
  public void testGetAnswerThatDoesntExist() throws ServerException, SQLException {
    HashMap<String, Object> args = new HashMap<>();

    args.put("id_val", "5f7d5b2c-ea5a-4c52-bb11-af70e16f3641");

    GetAnswerByIdCommand cmd = new GetAnswerByIdCommand(args);

    JsonObject result = (JsonObject) cmd.execute();
  }



}
