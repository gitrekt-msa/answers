package com.gitrekt.quora.commands.handlers;

import com.gitrekt.quora.exceptions.BadRequestException;
import com.gitrekt.quora.exceptions.ServerException;
import org.junit.Test;

import java.sql.SQLException;
import java.util.HashMap;

import static org.junit.Assert.*;

public class InsertAnswerCommandTest {

    String userId = "7b33e4fc-1a41-4661-a4d9-563fc21cd89e";
    String answerId = "91c9c754-33c9-11e9-b210-d663bd873d93";
    String questionId = "7b33e4fc-1a41-4661-a4d9-563fc21ce90f";
    String userId2 = "0d50fcd3-eed6-4774-8ac0-43c0c2b72b0d";

    @Test
    public void testValidInsert() {
        HashMap<String, Object> attributes = new HashMap<>();
        attributes.put("userId", userId);
        attributes.put("question_id_val", questionId);
        attributes.put("discussion_id_val", null);
        attributes.put("answer_text_val", "Test");
        attributes.put("is_public_val", false);
        attributes.put("stars_val", 3);
        attributes.put("media_val", "{\"paths\":[\"test/test\"]}");
        boolean failed = false;
        InsertAnswerCommand cmd = new InsertAnswerCommand(attributes);
        String msg = "";
        try {
            cmd.execute();
        } catch (Exception ex) {
            failed = true;
            msg = ex.getMessage();
        } catch (Error err){
            failed = true;
            msg = err.getMessage();
        }
        assertFalse(msg, failed);
    }


    @Test(expected = BadRequestException.class)
    public void testMissingUserId() throws ServerException, SQLException {
        HashMap<String, Object> attributes = new HashMap<>();

        attributes.put("question_id_val", questionId);
        attributes.put("discussion_id_val", null);
        attributes.put("id", answerId);
        attributes.put("answer_text_val", "Test");
        attributes.put("is_public_val", false);
        attributes.put("stars_val", 3);
        attributes.put("media_val", "{\"paths\":[\"test/test\"]}");

        InsertAnswerCommand cmd = new InsertAnswerCommand(attributes);

        cmd.execute();
    }


    @Test
    public void testEditAnswer() throws ServerException, SQLException {
        HashMap<String, Object> attributes = new HashMap<>();

        attributes.put("userId", userId);
        attributes.put("id", answerId);
        attributes.put("answer_text_val", "TestEdit");
        attributes.put("is_public_val", true);

        UpdateAnswerCommand cmd = new UpdateAnswerCommand(attributes);

        cmd.execute();
    }


    @Test(expected = SQLException.class)
    public void testEditAnswerWithoutPermission() throws ServerException, SQLException {
        HashMap<String, Object> attributes = new HashMap<>();

        attributes.put("userId", userId2);
        attributes.put("id", answerId);
        attributes.put("answer_text_val", "TestEdit");
        attributes.put("is_public_val", true);

        UpdateAnswerCommand cmd = new UpdateAnswerCommand(attributes);

        cmd.execute();
    }

}