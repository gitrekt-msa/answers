package com.gitrekt.quora.commands.handlers;

import com.gitrekt.quora.exceptions.ServerException;
import org.junit.Test;

import java.sql.SQLException;
import java.util.HashMap;

import static org.junit.Assert.*;

public class DeleteAnswerCommandTest {

    String answerId = "ac75f9de-33c8-11e9-b210-d663bd873d93";
    String userId = "0d50fcd3-eed6-4774-8ac0-43c0c2b72b0d";

    @Test
    public void testDeleteAnswer() throws ServerException, SQLException {
        HashMap<String, Object> args = new HashMap<>();
        args.put("id", answerId);
        args.put("userId", userId);

        DeleteAnswerCommand cmd = new DeleteAnswerCommand(args);

        cmd.execute();
    }

    @Test(expected = SQLException.class)
    public void testDeleteAnswerWithoutPermission() throws ServerException, SQLException {
        HashMap<String, Object> args = new HashMap<>();
        args.put("id", answerId);
        args.put("userId", "7b33e4fc-1a41-4661-a4d9-563fc21cd89e");

        DeleteAnswerCommand cmd = new DeleteAnswerCommand(args);

        cmd.execute();
    }

}