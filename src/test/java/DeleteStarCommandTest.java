import static org.mockito.AdditionalMatchers.not;
import static org.mockito.Mockito.*;

import com.gitrekt.quora.commands.handlers.DeleteStarCommand;
import com.gitrekt.quora.commands.Command;
import com.gitrekt.quora.database.postgres.handlers.StarPostgresHandler;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.UUID;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class DeleteStarCommandTest {

    private StarPostgresHandler starPostgresHandler;

    @Before
    public void initialize() {
        starPostgresHandler = mock(StarPostgresHandler.class);
    }

    @Test
    public void shouldAddStar()
            throws SQLException {
        HashMap<String, Object> map = new HashMap<>();

        map.put("userid","7b33e4fc-1a41-4661-a4d9-563fc21cd89e");
        map.put("answerid","91c9c754-33c9-11e9-b210-d663bd873d93");


        Command deletestarcommand = spy(new DeleteStarCommand (map));
        deletestarcommand.setPostgresHandler(starPostgresHandler);

        Object result = deletestarcommand.execute();

        verify(deletestarcommand, times(1).description("Should only check arguments once"))
                .checkArguments(new String[] {"userid", "answerid"});

        verify(starPostgresHandler, times(1).description("Should only delete star once"))
                .deleteStar((String)map.get("userid"),(String)map.get("answerid"));

        Assert.assertEquals("Should return a success message", "Answer deleted!", (String)result);

    }

}