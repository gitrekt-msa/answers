import static org.mockito.AdditionalMatchers.not;
import static org.mockito.Mockito.*;

import com.gitrekt.quora.commands.handlers.DeleteBestAnswerCommand;
import com.gitrekt.quora.commands.Command;
import com.gitrekt.quora.database.postgres.handlers.BestAnswerPostgresHandler;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.UUID;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class DeleteBestAnswerCommandTest {

    private BestAnswerPostgresHandler bestanswerpostgresHandler;

    @Before
    public void initialize() {
        bestanswerpostgresHandler = mock(BestAnswerPostgresHandler.class);
    }

    @Test
    public void shouldDeleteBestAnswer()
            throws SQLException {
        HashMap<String, Object> map = new HashMap<>();

        map.put("questionid","7b33e4fc-1a41-4661-a4d9-563fc21cd89e");
        map.put("userid","5f7d5b2c-ea5a-4c52-bb11-af70e16f3649");


        Command deletebestanswercommand = spy(new DeleteBestAnswerCommand(map));
        deletebestanswercommand.setPostgresHandler(bestanswerpostgresHandler);


        Object result = deletebestanswercommand.execute();

        verify(deletebestanswercommand, times(1).description("Should only check arguments once"))
                .checkArguments(new String[] {"questionid","userid"});

        verify(bestanswerpostgresHandler, times(1).description("Should only delete best answer once"))
                .DeleteBestAnswer((String)map.get("questionid"),(String)map.get("userid"));

        Assert.assertEquals("Should return a success message", "Best answer deleted successfully!", (String)result);

    }

}