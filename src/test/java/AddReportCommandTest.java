import static org.mockito.AdditionalMatchers.not;
import static org.mockito.Mockito.*;

import com.gitrekt.quora.commands.handlers.ReportAnswerCommand;
import com.gitrekt.quora.commands.Command;
import com.gitrekt.quora.database.postgres.handlers.ReportPostgresHandler;
import com.gitrekt.quora.exceptions.BadRequestException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.UUID;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class AddReportCommandTest {

    private ReportPostgresHandler reportPostgresHandler;

    @Before
    public void initialize() {
        reportPostgresHandler = mock(ReportPostgresHandler.class);
    }

    @Test
    public void ShouldAddReport()
            throws SQLException, BadRequestException {
        HashMap<String, Object> map = new HashMap<>();

        map.put("userid","7b33e4fc-1a41-4661-a4d9-563fc21cd89e");
        map.put("answerid","91c9c754-33c9-11e9-b210-d663bd873d93");
        map.put("reporttext","spam");


        Command reportanswercommand = spy(new ReportAnswerCommand (map));
        reportanswercommand.setPostgresHandler(reportPostgresHandler);

        Object result = reportanswercommand.execute();

        verify(reportanswercommand, times(1).description("Should only check arguments once"))
                .checkArguments(new String[] {"userid", "answerid","reporttext"});

        verify(reportPostgresHandler, times(1).description("Should only add report once"))
                .addReport((String)map.get("userid"),(String)map.get("answerid"),(String)map.get("reporttext"));

        Assert.assertEquals("Should return a success message", "Report Added!", (String)result);

    }

}