import static org.mockito.AdditionalMatchers.not;
import static org.mockito.Mockito.*;

import com.gitrekt.quora.commands.handlers.InsertBestAnswerCommand;
import com.gitrekt.quora.commands.Command;
import com.gitrekt.quora.database.postgres.handlers.BestAnswerPostgresHandler;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.UUID;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class InsertBestAnswerCommandTest {

  private BestAnswerPostgresHandler bestanswerpostgresHandler;

  @Before
  public void initialize() {
    bestanswerpostgresHandler = mock(BestAnswerPostgresHandler.class);
  }

  @Test
  public void shouldInsertBestAnswer()
          throws SQLException {
    HashMap<String, Object> map = new HashMap<>();

    map.put("questionid","5f7d5b2c-ea5a-4c52-bb11-af70e16f3649");
    map.put("answerid","ac75f9de-33c8-11e9-b210-d663bd873d93");
    map.put("userid", "7b33e4fc-1a41-4661-a4d9-563fc21cd89e");

    Command insertbestanswercommand = spy(new InsertBestAnswerCommand (map));
    insertbestanswercommand.setPostgresHandler(bestanswerpostgresHandler);

    Object result = insertbestanswercommand.execute();

    verify(insertbestanswercommand, times(1).description("Should only check arguments once"))
            .checkArguments(new String[] {"questionid", "answerid","userid"});

    verify(bestanswerpostgresHandler, times(1).description("Should only insert best answer once"))
            .InsertBestAnswer((String)map.get("questionid"),(String)map.get("answerid"),(String)map.get("userid"));

    Assert.assertEquals("Should return a success message", "Best answer inserted successfully!", (String)result);

  }

}